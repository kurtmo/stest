//
//  RequestHelper.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/26/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

final class RequestHelper: NSObject {

    static func getTranslatorRequest() -> URLRequest {
        
        let url: URL = URL(string: "https://lc-api.sdl.com/translate")!
        var request: URLRequest = URLRequest(url: url)
        
        let headerValue = "LC apiKey=" + GlobalConstants.ApiKey.translatorApiKey
        request.setValue(headerValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.httpMethod = "POST"
        
        return request
    }
}
