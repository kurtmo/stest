//
//  Translator.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/26/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

typealias TranslatorCompletionHandler = (_ model: Translation) -> Void

final class Translator: NSObject {

    static func makeTranslation(inputLanguage: GlobalConstants.Language,
                                outputLanguage: GlobalConstants.Language,
                                inputText: String,
                                completionHandler: @escaping TranslatorCompletionHandler,
                                failureHandler: @escaping FailureHandler){
        
        let initialData = ["text": inputText, "from": inputLanguage.rawValue, "to": outputLanguage.rawValue]
        ServerManager.sendRequestWithData(data: initialData, completionHandler: { data in
            
            let translate = Translation(context: PersistenceService.context)
            translate.setupData(initialData)
            translate.addOutputText(dictionary: data)
            PersistenceService.saveContext()
            
            DispatchQueue.main.async {
                completionHandler(translate)
            }
        }) { error in
            DispatchQueue.main.async {
                failureHandler(error)
            }
        }
    }
}

