//
//  ServerManager.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/25/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

typealias FailureHandler = (_ error: String) -> Void
typealias CompletionHandler = (_ data: [String: Any]) -> Void

final class ServerManager: NSObject {
    
    static private let defaultSession = URLSession(configuration: .default)
    static private var dataTask: URLSessionDataTask?
    
    static func sendRequestWithData(data: [String: String],
                                    completionHandler: @escaping CompletionHandler,
                                    failureHandler: @escaping FailureHandler) {
        
        if Reachability.isConnectedToNetwork(){
            dataTask?.cancel()
            var request = RequestHelper.getTranslatorRequest()
            let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            guard let requestBody = jsonData else {
                return
            }
            
            request.httpBody = requestBody
            dataTask = defaultSession.dataTask(with: request) { data, response, error in
                defer { self.dataTask = nil }
                
                if let error = error {
                    DispatchQueue.main.async {
                        failureHandler(error.localizedDescription)
                    }
                }
                guard let dataResponse = data else {
                    return
                }
                
                do {
                    if let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                        DispatchQueue.main.async {
                            completionHandler(json)
                        }
                        print(json)
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        failureHandler(error.localizedDescription)
                    }
                }
            }
            
            dataTask?.resume()
        }else{
            DispatchQueue.main.async {
                failureHandler("Internet Connection not Available!")
            }
        }
    }
}

