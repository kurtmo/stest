//
//  Translation+CoreDataProperties.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/27/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//
//

import Foundation
import CoreData


extension Translation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Translation> {
        return NSFetchRequest<Translation>(entityName: "Translation")
    }

    @NSManaged public var inputText: String?
    @NSManaged public var outputText: String?
    @NSManaged public var inputLang: String?
    @NSManaged public var outputLang: String?

    func setupData(_ dictionary: [String: Any]?) {
        guard let initialData = dictionary else {
            return
        }
        inputText = initialData[GlobalConstants.DictionaryKeys.inputTextKey] as? String
        inputLang = initialData[GlobalConstants.DictionaryKeys.inputLanguageKey] as? String
        outputLang = initialData[GlobalConstants.DictionaryKeys.outputLanguageKey] as? String
    }
    
    func addOutputText(dictionary: [String: Any]) {
        outputText = dictionary[GlobalConstants.DictionaryKeys.outputTextKey] as? String
    }
    
    func getTranslationTest() -> String {
        if let inputText = inputText, let outputText = outputText {
            return ("\(inputText)" + " - " + "\(outputText)")
        } else {
            return ""
        }
    }
}
