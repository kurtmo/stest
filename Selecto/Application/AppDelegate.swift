//
//  AppDelegate.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/25/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        start()

        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        PersistenceService.saveContext()
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let profileVc = storyboard.instantiateViewController(withIdentifier: GlobalConstants.ViewControllers.profileVCKey)
        profileVc.tabBarItem = UITabBarItem(title: "Profile", image: nil, tag: 0)
        let translatorVc = storyboard.instantiateViewController(withIdentifier: GlobalConstants.ViewControllers.translatorVCKey)
        translatorVc.tabBarItem = UITabBarItem(title: "Translator", image: nil, tag: 1)
        let gifsVc = storyboard.instantiateViewController(withIdentifier: GlobalConstants.ViewControllers.gifsVCKey)
        gifsVc.tabBarItem = UITabBarItem(title: "Gifs", image: nil, tag: 2)
        
        let tabBar = UITabBarController()
        window?.rootViewController = tabBar
        window?.makeKeyAndVisible()
        
        tabBar.setViewControllers([profileVc, translatorVc, gifsVc], animated: false)
    }
}

