//
//  ProfileCollectionViewCell.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/26/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

final class ProfileCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var featuredView: CellSelectedView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    // MARK: cell setup
    func setupCell(index: NSInteger) {
        if index == 0 {
            featuredView.isHidden = false
        }
    }
}
