//
//  ProfileViewController.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/26/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

private let collectionViewCellIdentifier = "cell"

final class ProfileViewController: UIViewController {

    @IBOutlet weak private var borderImageView: UIImageView!
    @IBOutlet weak private var profileImageView: UIImageView!
    
    @IBOutlet weak private var followButton: UIButton!
    @IBOutlet weak private var shareButton: UIButton!
    
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var topView: UIView!
    @IBOutlet private var statisticViews: [StatisticView]!
    @IBOutlet weak private var questionLabel: UILabel!
    
    // MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTopView()
        setupCollectionView()
        setupStatisticViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupCollectionView()
    }

    // MARK: view setup
    func setupTopView() {
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
        profileImageView.addBorder(color: .white, width: 2.0)
        borderImageView.layer.cornerRadius = profileImageView.layer.cornerRadius
        
        let borderColor = GlobalConstants.Colors.borderColor
        let buttonsArray: [UIButton] = [followButton, shareButton]
        for button in buttonsArray {
            button.addBorder(color: borderColor , width: 1.5)
            button.layer.cornerRadius = 17.0
        }
        
        questionLabel.text = "Do You Want To Go Out With Me?\n (A) Yes (B) A (C) B."
        
        topView.addBorder(color: .lightGray, width: 0.5)
        collectionView.addBorder(color: .lightGray, width: 0.5)
    }
    
    func setupStatisticViews() {
        for (index, statisticView) in statisticViews.enumerated() {
            statisticView.setupView(index: index)
        }
    }
    
    func setupCollectionView() {
        let nib = UINib(nibName: GlobalConstants.Nibs.ProfileCollectionViewCell, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: collectionViewCellIdentifier)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)
        
        let cellWidth = (collectionView.frame.size.width - 45) / 3.0
        let cellHeight = (collectionView.frame.size.height) / 2.8
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
    }
}


extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellIdentifier, for: indexPath) as! ProfileCollectionViewCell
        cell.setupCell(index: indexPath.row)
        
        return cell
    }
}

