//
//  StatisticView.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/26/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

final class StatisticView: ParentView {
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var themeLabel: UILabel!
            
    // MARK: setup
    func setupView(index: NSInteger) {
        view.addBorder(color: UIColor.lightGray, width: 0.5)
        backgroundColor = UIColor.white
        
        var valueString = ""
        var themeString = ""
        switch index {
        case 0:
            valueString = "180"
            themeString = "Pages"
        case 1:
            valueString = "1244"
            themeString = "Followers"
        case 2:
            valueString = "12455"
            themeString = "Following"
        default:
            break
        }
        valueLabel.text = valueString
        themeLabel.text = themeString
    }

}
