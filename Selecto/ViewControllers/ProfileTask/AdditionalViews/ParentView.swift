//
//  ParentView.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/27/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

class ParentView: UIView {
    
    var view: UIView!

    // MARK: init
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func xibSetup() {
        view = loadNib()
        view.frame = bounds
        addSubview(view)
    }
}
