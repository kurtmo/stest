//
//  GifsViewController.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/29/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

final class GifsViewController: UIViewController {

    let gifsNames = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: .startAnimationNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: .stopAnimationNotification, object: nil)
    }
}

extension GifsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gifsNames.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GifCollectionViewCell
        let gifName = gifsNames[indexPath.row]
        cell.setupCell(gifName: gifName)

        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: .startAnimationNotification, object: nil)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: .stopAnimationNotification, object: nil)
    }
}
