//
//  GifCollectionViewCell.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/29/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit
import SwiftyGif

final class GifCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var notificationCenter = NotificationCenter.default
    
    deinit {
        notificationCenter.removeObserver(self, name: .stopAnimationNotification, object: nil)
        notificationCenter.removeObserver(self, name: .startAnimationNotification, object: nil)
    }
    
    func setupCell(gifName: String) {
        let gifManager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: gifName)
        imageView.setGifImage(gif, manager: gifManager)
        
        notificationCenter.addObserver(self, selector: #selector(stopAnimating), name: .stopAnimationNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(startAnimating), name: .startAnimationNotification, object: nil)
    }
    
    @objc
    func stopAnimating() {
        imageView.stopAnimatingGif()
    }
    
    @objc
    func startAnimating() {
        imageView.startAnimatingGif()
    }
}
