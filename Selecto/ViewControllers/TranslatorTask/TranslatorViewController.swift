//
//  ViewController.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/25/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit
import CoreData

final class TranslatorViewController: UIViewController {
    
    @IBOutlet weak private var originalLanguageLabel: UILabel!
    @IBOutlet weak private var outputLanguageLabel: UILabel!
    
    @IBOutlet weak private var changeLanguageButton: UIButton!
    @IBOutlet weak private var translateButton: UIButton!
    
    @IBOutlet weak private var inputTextView: UITextView!
    @IBOutlet weak private var outputTextView: UITextView!
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var inputLanguage: GlobalConstants.Language = .en
    private var outputLanguage: GlobalConstants.Language = .ua
    private var dataSource = [Translation]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        getCoreDataEntitites()
    }
    
    // MARK: view setup
    private func setupView() {
        translateButton.addBorder(color: UIColor.blue, width: 1.0)
        changeLanguageButton.addBorder(color: UIColor.blue, width: 1.0)
        
        let textViewsArray: [UITextView] = [inputTextView, outputTextView]
        for textView in textViewsArray {
            textView.textColor = UIColor.darkGray
            textView.addBorder(color: UIColor.lightGray, width: 1.0)
        }
        
        tableView.tableFooterView = UIView()
        setupLabelsText()
    }
    
    private func setupLabelsText() {
        originalLanguageLabel.text = inputLanguage.rawValue
        outputLanguageLabel.text = outputLanguage.rawValue
    }
    
    private func getCoreDataEntitites() {
        let fetchRequest: NSFetchRequest<Translation> = Translation.fetchRequest()
        do {
            dataSource = try PersistenceService.context.fetch(fetchRequest)
        } catch { }
    }

    // MARK: actions
    @IBAction func translateAction(_ sender: Any) {
        view.endEditing(true)
        outputTextView.text = ""
        activityIndicator.startAnimating()
        
        Translator.makeTranslation(inputLanguage: inputLanguage, outputLanguage: outputLanguage, inputText: inputTextView.text,
                                   completionHandler: { [weak self] translateModel in
                                    
                                    guard let weakSelf = self else {
                                        return
                                    }
                                    weakSelf.activityIndicator.stopAnimating()
                                    weakSelf.outputTextView.text = translateModel.outputText
                                    weakSelf.dataSource.append(translateModel)
        }) { [weak self] error in
            guard let weakSelf = self else {
                return
            }
            weakSelf.outputTextView.text = ("Error: \(error)")
            weakSelf.textViewDidEndEditing(weakSelf.outputTextView)
        }
    }
    
    @IBAction func changeLanguageAction(_ sender: Any) {
        let tempVar = inputLanguage
        inputLanguage = outputLanguage
        outputLanguage = tempVar
        setupLabelsText()
    }
}

extension TranslatorViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let count = dataSource.count - 1
        let translation = dataSource[count - indexPath.row]
        
        cell.textLabel?.text = translation.getTranslationTest()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.isSelected = false
    }
}

extension TranslatorViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        let placeholderText = textView.tag == 0 ? "Text" : "Translation"
        if textView.text.isEmpty {
            textView.text = placeholderText
            textView.textColor = UIColor.darkGray
        }
    }
}
