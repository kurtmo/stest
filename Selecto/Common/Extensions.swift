//
//  Extensions.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/26/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

extension UIView {
    
    func addBorder(color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
}

extension Notification.Name {
    static let stopAnimationNotification = Notification.Name(
        rawValue: "stopAnimation")
    static let startAnimationNotification = Notification.Name(
        rawValue: "startAnimation")
}
