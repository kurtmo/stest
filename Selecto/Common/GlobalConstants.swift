//
//  GlobalConstants.swift
//  Selecto
//
//  Created by Max Ostapchuk on 1/25/18.
//  Copyright © 2018 Max Ostapchuk. All rights reserved.
//

import UIKit

struct GlobalConstants {
    
    struct ApiKey {
        static let translatorApiKey = "U90JuGbAG5bQ88DNjAurDQ%3D%3D"
    }
    
    struct DictionaryKeys {
        static let inputTextKey = "text"
        static let outputTextKey = "translation"
        static let inputLanguageKey = "from"
        static let outputLanguageKey = "to"
    }
    
    struct Colors {
        static let borderColor = UIColor(red: 55.0 / 255.0, green: 181.0 / 255.0, blue: 190.0 / 255.0, alpha: 1.0)
    }
    
    struct ViewControllers {
        static let translatorVCKey = "TranslatorVC"
        static let profileVCKey = "ProfileVC"
        static let gifsVCKey = "GifsVC"
    }
    
    struct Nibs {
        static let ProfileCollectionViewCell = "ProfileCollectionViewCell"
    }
    
    enum Language: String {
        case en = "eng"
        case ua = "ukr"
    }
}

